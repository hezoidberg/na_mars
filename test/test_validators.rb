require 'minitest/autorun'
require 'validators/plateau_input_validator'
require 'validators/rover_input_validator'
require 'validators/command_input_validator'
require 'validators/leading_zero_validator'

class TestValidators < Minitest::Test
  def test_plateau_validator
    assert(Validators::PlateauInputValidator.valid?('5 5'))
    assert(!Validators::PlateauInputValidator.valid?('5 5 4'))
  end

  def test_rover_validator
    assert(Validators::RoverInputValidator.valid?('1 2 N'))
    assert(!Validators::RoverInputValidator.valid?('1 2 F'))
    assert(!Validators::RoverInputValidator.valid?('-1 2 N'))
  end

  def test_command_validator
    assert(Validators::CommandInputValidator.valid?('LLRMLLMM'))
    assert(!Validators::CommandInputValidator.valid?('LLRSMLLMM'))
  end

  def test_leading_zero_validator
    assert(Validators::LeadingZeroValidator.valid?('123'))
    assert(!Validators::LeadingZeroValidator.valid?('00123'))
  end
end
