require 'minitest/autorun'
require 'rover'
require 'plateau'
require 'point'
require 'enums/direction'
require 'commands/invalid_command_error'
require 'commands/move'

class TestMove < Minitest::Test
  def setup
    @plateau = Plateau.new(5, 5)
  end

  def test_direction_has_not_changed
    rover = Rover.new(Point.new(1, 1), Enums::Direction::N)

    assert_equal Enums::Direction::N, rover.direction
    Commands::Move.new(rover, @plateau).execute
    assert_equal Enums::Direction::N, rover.direction
  end

  def test_reach_north_border
    rover = Rover.new(Point.new(1, 5), Enums::Direction::N)
    err = assert_raises Commands::InvalidCommandError do
      Commands::Move.new(rover, @plateau).execute
    end
    assert_equal('North border of the plateau reached', err.message)
  end

  def test_reach_east_border
    rover = Rover.new(Point.new(5, 1), Enums::Direction::E)
    err = assert_raises Commands::InvalidCommandError do
      Commands::Move.new(rover, @plateau).execute
    end
    assert_equal('East border of the plateau reached', err.message)
  end

  def test_reach_south_border
    rover = Rover.new(Point.new(2, 0), Enums::Direction::S)
    err = assert_raises Commands::InvalidCommandError do
      Commands::Move.new(rover, @plateau).execute
    end
    assert_equal('South border of the plateau reached', err.message)
  end

  def test_reach_west_border
    rover = Rover.new(Point.new(0, 4), Enums::Direction::W)
    err = assert_raises Commands::InvalidCommandError do
      Commands::Move.new(rover, @plateau).execute
    end
    assert_equal('West border of the plateau reached', err.message)
  end

  def test_blocked_moving
    rover = Rover.new(Point.new(2, 2), Enums::Direction::W)
    new_rover = Rover.new(Point.new(2, 1), Enums::Direction::N)

    @plateau.add_rover(rover)
    err = assert_raises Commands::InvalidCommandError do
      Commands::Move.new(new_rover, @plateau).execute
    end
    assert_equal('Moving is blocked', err.message)
  end
end
