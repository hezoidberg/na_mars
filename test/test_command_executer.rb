require 'minitest/autorun'
require 'plateau'
require 'point'
require 'rover'
require 'commands/command_executer'
require 'commands/invalid_command_error'
require 'enums/direction'

class TestCommandExecuter < Minitest::Test
  def setup
    @plateau = Plateau.new(5, 5)
  end

  def test_rover_moving_left
    rover = Rover.new(Point.new(1, 2), Enums::Direction::N)
    command_executer = Commands::CommandExecuter.new(rover, @plateau)
    'LMLMLMLMM'.each_char { |command| command_executer.execute(command) }
    assert_equal(rover.direction, Enums::Direction::N)
    assert_equal(rover.position, Point.new(1, 3))
  end

  def test_rover_moving_right
    rover = Rover.new(Point.new(3, 3), Enums::Direction::E)
    command_executer = Commands::CommandExecuter.new(rover, @plateau)
    'MMRMMRMRRM'.each_char { |command| command_executer.execute(command) }
    assert_equal(rover.direction, Enums::Direction::E)
    assert_equal(rover.position, Point.new(5, 1))
  end

  def test_invalid_command
    rover = Rover.new(Point.new(3, 3), Enums::Direction::E)
    command_executer = Commands::CommandExecuter.new(rover, @plateau)
    err = assert_raises Commands::InvalidCommandError do
      'MMRMMRFMRRM'.each_char { |command| command_executer.execute(command) }
    end
    assert_equal('Not supported command', err.message)
  end
end
