require 'minitest/autorun'
require 'rover'
require 'point'
require 'enums/direction'
require 'enums/rotate_direction'
require 'commands/rotate'

class TestRotate < Minitest::Test
  def setup
    @rover = Rover.new(Point.new(1, 1), Enums::Direction::N)
  end

  def test_coordinates_have_not_changed
    assert_equal Point.new(1, 1), @rover.position

    Commands::Rotate.new(@rover, Enums::RotateDirection::L).execute
    assert_equal Point.new(1, 1), @rover.position
  end

  def test_rotate_right
    assert_equal Enums::Direction::N, @rover.direction

    Commands::Rotate.new(@rover, Enums::RotateDirection::R).execute
    assert_equal Enums::Direction::E, @rover.direction
  end

  def test_rotate_left
    assert_equal Enums::Direction::N, @rover.direction

    Commands::Rotate.new(@rover, Enums::RotateDirection::L).execute
    assert_equal Enums::Direction::W, @rover.direction
  end

  def test_right_then_left
    assert_equal Enums::Direction::N, @rover.direction

    Commands::Rotate.new(@rover, Enums::RotateDirection::R).execute
    Commands::Rotate.new(@rover, Enums::RotateDirection::L).execute
    assert_equal Enums::Direction::N, @rover.direction
  end

  def test_180_degrees_rotate
    Commands::Rotate.new(@rover, Enums::RotateDirection::R).execute.execute
    assert_equal Enums::Direction::S, @rover.direction
  end

  def test_360_degrees_rotate
    assert_equal Enums::Direction::N, @rover.direction
    rotate = Commands::Rotate.new(@rover, Enums::RotateDirection::R)
    4.times { rotate.execute }
    assert_equal Enums::Direction::N, @rover.direction
  end
end
