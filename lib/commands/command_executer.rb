require 'enums/rotate_direction'
require 'commands/move'
require 'commands/rotate'
require 'commands/invalid_command_error'

module Commands
  # Class responsible for rover moving
  class CommandExecuter
    attr_reader :rover, :plateau

    def initialize(rover, plateau)
      @rover = rover
      @plateau = plateau
    end

    def execute(str_command)
      move_rover_command(str_command).execute
      self
    end

    private

    def move_rover_command(str_command)
      case str_command
      when 'M'
        Move.new(rover, plateau)
      when 'L'
        Rotate.new(rover, Enums::RotateDirection::L)
      when 'R'
        Rotate.new(rover, Enums::RotateDirection::R)
      else
        raise InvalidCommandError, 'Not supported command'
      end
    end
  end
end
