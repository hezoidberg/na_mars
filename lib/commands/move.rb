require 'enums/direction'

module Commands
  # Moves rover forward one step
  class Move
    attr_reader :rover, :plateau
    attr_reader :step

    def initialize(rover, plateau, step = 1)
      @rover = rover
      @plateau = plateau
      @step = step
    end

    def execute
      move
      check_blockers
      self
    end

    private

    def move
      case rover.direction
      when Enums::Direction::N
        move_north
      when Enums::Direction::W
        move_west
      when Enums::Direction::S
        move_south
      when Enums::Direction::E
        move_east
      end
    end

    def current_position
      rover.position
    end

    def existing_positions
      positions ||= plateau.rovers.map(&:position)
      positions
    end

    def moving_blocked?
      existing_positions.include? current_position
    end

    def check_blockers
      raise InvalidCommandError, 'Moving is blocked' if moving_blocked?
    end

    def move_north
      if current_position.y == plateau.height
        raise InvalidCommandError, 'North border of the plateau reached'
      end
      current_position.y = current_position.y + step
    end

    def move_west
      if current_position.x.zero?
        raise InvalidCommandError, 'West border of the plateau reached'
      end
      current_position.x = current_position.x - step
    end

    def move_south
      if current_position.y.zero?
        raise InvalidCommandError, 'South border of the plateau reached'
      end
      current_position.y = current_position.y - step
    end

    def move_east
      if current_position.x == plateau.width
        raise InvalidCommandError, 'East border of the plateau reached'
      end
      current_position.x = current_position.x + step
    end
  end
end
