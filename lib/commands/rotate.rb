require 'enums/direction'
require 'enums/rotate_direction'

module Commands
  # Makes the rover spin 90 degrees left or right
  class Rotate
    DIRECTIONS = [
      Enums::Direction::N,
      Enums::Direction::W,
      Enums::Direction::S,
      Enums::Direction::E
    ].freeze

    attr_reader :rover
    attr_reader :rotate_direction

    def initialize(rover, rotate_direction)
      @rover = rover
      @rotate_direction = rotate_direction
    end

    def execute
      rotate_left if rotate_direction == Enums::RotateDirection::L
      rotate_right if rotate_direction == Enums::RotateDirection::R
      self
    end

    private

    def rotate_left
      rover.direction = if rover.direction == Enums::Direction::E
                          Enums::Direction::N
                        else
                          DIRECTIONS[DIRECTIONS.find_index(rover.direction) + 1]
                        end
    end

    def rotate_right
      rover.direction = if rover.direction == Enums::Direction::N
                          Enums::Direction::E
                        else
                          DIRECTIONS[DIRECTIONS.find_index(rover.direction) - 1]
                        end
    end
  end
end
