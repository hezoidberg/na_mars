# Rectangular grid
class Plateau
  attr_reader :width, :height
  attr_reader :rovers

  def initialize(width, height)
    @width = width
    @height = height
    @rovers = []
  end

  def add_rover(rover)
    rovers << rover
  end
end
