module Validators
  class InputValidator
    attr_reader :input

    def self.valid?(input)
      new(input).valid?
    end

    def self.validation_regexp
      raise NotImplementedError
    end

    def initialize(input)
      @input = input
    end

    def valid?
      self.class.validation_regexp.match?(input)
    end
  end
end
