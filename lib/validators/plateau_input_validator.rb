require 'validators/input_validator'

module Validators
  class PlateauInputValidator < InputValidator
    def self.validation_regexp
      /^\d+\s\d+$/
    end
  end
end
