require 'validators/input_validator'

module Validators
  class CommandInputValidator < InputValidator
    def self.validation_regexp
      /^[LRM]+$/
    end
  end
end
