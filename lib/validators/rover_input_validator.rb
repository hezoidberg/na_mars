require 'validators/input_validator'

module Validators
  class RoverInputValidator < InputValidator
    def self.validation_regexp
      /^\d+\s\d+\s[NWSE]$/
    end
  end
end
