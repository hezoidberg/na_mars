require 'validators/input_validator'

module Validators
  class LeadingZeroValidator < InputValidator
    def self.validation_regexp
      /^0/
    end

    def valid?
      !super
    end
  end
end
