module Enums
  module Direction
    N = 'N'.freeze
    W = 'W'.freeze
    S = 'S'.freeze
    E = 'E'.freeze

    MAPPING = { 'N' => N, 'W' => W, 'S' => S, 'E' => E }.freeze

    def self.from_str(str)
      MAPPING[str]
    end
  end
end
