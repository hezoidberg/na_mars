#!/usr/bin/env ruby
$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'validators/plateau_input_validator'
require 'validators/leading_zero_validator'
require 'validators/rover_input_validator'
require 'validators/command_input_validator'
require 'commands/command_executer'
require 'enums/direction'
require 'plateau'
require 'point'
require 'rover'

def split_input(input)
  input.split(' ')
end

def stop_if_invalid
  p 'Invalid plateau input'
  abort
end

def plateao_validate(input)
  return true if Validators::PlateauInputValidator.valid?(input)
  stop_if_invalid
end

def plateau_format_validate(array_of_plateao_coords)
  array_of_plateao_coords.each do |param|
    next if Validators::LeadingZeroValidator.valid?(param)
    stop_if_invalid
  end
end

def rover_input_validate(input)
  return true if Validators::RoverInputValidator.valid?(input)
  stop_if_invalid
end

def rover_format_validate(array_of_rover_params)
  array_of_rover_params[0..1].each do |param|
    next if Validators::LeadingZeroValidator.valid?(param)
    stop_if_invalid
  end
end

def commands_validate(input)
  return true if Validators::CommandInputValidator.valid?(input)
  stop_if_invalid
end

def run_commands(commands, rover, plateau)
  command_executer = Commands::CommandExecuter.new(rover, plateau)
  commands.each_char { |command| command_executer.execute(command) }
end

def print_result(plateau)
  plateau.rovers.each { |rover| puts rover }
end

def init_plateau
  p 'Please enter plateau width and height:'
  plateau_size = gets.strip
  plateao_validate(plateau_size)
  array_of_plateao_coords = split_input(plateau_size)
  plateau_format_validate(array_of_plateao_coords)
  Plateau.new(array_of_plateao_coords[0].to_i, array_of_plateao_coords[1].to_i)
end

def rover_blank?(new_rover)
  new_rover.nil? || new_rover.empty?
end

def create_rover(input_params_array)
  position = Point.new(input_params_array[0].to_i,
                       input_params_array[1].to_i)
  direction = Enums::Direction.from_str(input_params_array[2])

  Rover.new(position, direction)
end

def init_rover
  p 'Please enter rover position and direction:'
  rover_input = gets.strip
  return nil if rover_blank?(rover_input)

  rover_input_validate(rover_input)
  array_of_rover_params = split_input(rover_input)
  rover_format_validate(array_of_rover_params)
  create_rover(array_of_rover_params)
end

def init_commands
  p 'Please enter commands <available - LRM>:'
  commands_input = gets.strip
  commands_validate(commands_input)
  commands_input
end

def execute_commands_loop(plateau)
  loop do
    rover = init_rover
    if rover.nil?
      print_result(plateau)
      break
    end
    commands = init_commands
    run_commands(commands, rover, plateau)
    plateau.add_rover(rover)
  end
end

def run
  plateau = init_plateau

  execute_commands_loop(plateau)
end

run
